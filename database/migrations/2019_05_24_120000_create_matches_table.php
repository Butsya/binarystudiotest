<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->bigIncrements('match_id');
            $table->bigInteger('first_team_id', false, true);
            $table->bigInteger('second_team_id', false, true);
            $table->tinyInteger('first_team_result', false, true)->default(0);
            $table->tinyInteger('second_team_result', false, true)->default(0);
            $table->tinyInteger('group_id', false, true);
            $table->unique(['first_team_id', 'second_team_id']);
            $table->foreign('first_team_id')->references('team_id')->on('teams')->onDelete('cascade');
            $table->foreign('second_team_id')->references('team_id')->on('teams')->onDelete('cascade');
            $table->foreign('group_id')->references('group_id')->on('groups')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matches');
    }
}
