<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'GroupController@show');

Route::post('/group', 'GroupController@add');

Route::delete('/group/{groupName}', 'GroupController@delete');

Route::get('/group/{groupName}', 'TeamController@show');

Route::post('/team', [
        'uses' => 'TeamController@add',
        'as' => 'addTeam',
    ]
);

Route::delete('/team', 'TeamController@delete');

Route::post('/match/generate', 'MatchController@generate');

Route::post('/match/write', 'MatchController@write');
