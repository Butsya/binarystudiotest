<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{csrf_token()}}" />

    <title>App</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <style>
        .app-container {
            display: flex;
            justify-content: center;
            padding: 3%;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .groups-list-container ul {
            padding-left: 1rem;
        }

        .flex-col {
            display: flex;
            flex-direction: column;
        }

        ul {
            display: block;
            list-style-type: disc;
            margin-block-start: 1em;
            margin-block-end: 1em;
            margin-inline-start: 0px;
            margin-inline-end: 0px;
            padding-inline-start: 40px;
        }

        .group-item {
            margin: 5px 0;
            -webkit-transition: .3s;
            transition: .3s;
        }

        * {
            box-sizing: border-box;
        }

        li {
            display: list-item;
            text-align: -webkit-match-parent;
        }

        ul {
            list-style-type: disc;
        }

        body {
            margin: 0;
            padding: 0;
            font-family: sans-serif;
        }

        html {
            color: -internal-root-color;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .group-list-container .button-container {
            margin-left: 10px;
        }

        .button-container {
            display: flex;
            align-items: center;
            justify-content: center;
            color: #fff;
            background-color: #228b22;
            min-width: 100px;
            padding: 5px;
            cursor: pointer;
            border-radius: 4px;
            -webkit-transition: background-color .5s ease;
            transition: background-color .5s ease;
        }
    </style>
</head>
<body>
    <div class="app-container">
        <div tabindex="-1" role="group" style="outline: none;">
            <div class="group-list-container">
                    <div class="flex-center">
                        <span>Groups:</span>
                        <div class="button-container"
                        @if (count($groups) === 26)
                            style="background-color: rgb(170, 170, 170);"
                        @endif
                        >
                            <span>New</span>
                        </div>
                    </div>
                <ul class="flex-col">
                    <div class="group-items">
                        @foreach($groups as $groupName)
                            <li class="group-item links">
                                <a href="/group/{{ $groupName }}">{{ $groupName }}</a>
                                <span class="icon deleteGroup"
                                      data-name="{{ $groupName }}"
                                      data-token="{{ csrf_token() }}"
                                      style="cursor: pointer; display: none;"
                                >x</span>
                            </li>
                        @endforeach
                    </div>
                </ul>
            </div>
        </div>
    </div>

    <script src="http://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous">
    </script>
    <script>
        $(document).ready(function(){
            $('.group-item').hover(
                function(){$(this).children('.icon').show();},
                function(){$(this).children('.icon').hide();}
            );
            $('.button-container').click(function(e){
                e.preventDefault();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{ url('/group') }}",
                    method: 'post',
                    success: function(result){
                        if (result !== "") {
                            $('.group-items').append(
                                '<li class="group-item">' +
                                '<a href="/group/' + result + '">' + result + '</a>' +
                                '<span class="icon deleteGroup" ' +
                                'data-name="' + result + '"' +
                                'style="cursor: pointer; display: none;">x</span>' +
                                '</li>'
                            )
                            window.location.reload();
                        };
                    }});
            });
            $('.deleteGroup').click(function (){
                let groupName = $(this).data('name');
                let token = $(this).data('token');
                let el = this;
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                $.ajax({
                    data: {
                        "groupName": groupName,
                        "token": token,
                    },
                    url: 'group/' + groupName,
                    method: 'delete',
                    success: function(success) {
                        if (success === groupName) {
                            $(el).closest('li').remove();
                            window.location.reload();
                        }
                    }});
            });
        });
    </script>
</body>
</html>
