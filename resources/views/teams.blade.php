<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{ csrf_token() }}" />

    <title>App</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <style>
        .app-container {
            display: flex;
            justify-content: center;
            padding: 3%;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .group-container .flex-col {
            margin: 20px 0;
        }

        .flex-col {
            display: flex;
            flex-direction: column;
        }

        .flex-between {
            justify-content: space-between;
        }

        .flex, .flex-between {
            display: flex;
            align-items: center;
        }

        .new-team-controls .input-container {
            flex: 1 1;
        }

        .input-container {
            margin: 0 10px;
        }

        .new-team-controls .input-container input {
            width: 100%;
        }

        .input-container.invalid input {
            box-shadow: 0 0 3px 0 #8b0000;
        }

        .input-container input {
            outline: none;
            border: 1px solid #a9a9a9;
            padding: 5px;
            border-radius: 4px;
        }

        .team-list .icon {
            position: absolute;
            left: -2.5rem;
            top: 50%;
        }

        .icon {
            font-size: 1.2rem;
            line-height: 0;
        }

        .team-list .team-item {
            position: relative;
            margin-bottom: 5px;
            -webkit-transition: .3s;
            transition: .3s;
        }

        li {
            display: list-item;
            text-align: -webkit-match-parent;
        }

        ol {
            list-style-type: decimal;
        }

        .match-list {
            max-height: 40vh;
            overflow-y: auto;
        }

        .match-item:not(:last-child) {
            margin-bottom: 5px;
        }

        .match-list .match-team:first-child {
            text-align: right;
        }

        .match-list .match-team {
            max-width: 50%;
            overflow-x: hidden;
            text-overflow: ellipsis;
        }

        .flex-grow {
            flex: 1 1;
        }

        .match-list input {
            text-align: center;
            width: 40px;
        }

        * {
            box-sizing: border-box;
        }

        body {
            margin: 0;
            padding: 0;
            font-family: sans-serif;
        }

        html {
            color: -internal-root-color;
        }

        .button-container {
            display: flex;
            align-items: center;
            justify-content: center;
            color: #fff;
            background-color: #228b22;
            min-width: 100px;
            padding: 5px;
            cursor: pointer;
            border-radius: 4px;
            -webkit-transition: background-color .5s ease;
            transition: background-color .5s ease;
        }
    </style>
</head>
<body>
    <div class="app-container">
        <div tabindex="-1" role="group" style="outline: none;">
            <div class="page group-container">
                <form action="{{ route('addTeam') }}" method="post">
                    <a href="/">
                        <span class="icon" style="cursor: inherit">⟵</span>
                        <span>Go Back</span>
                    </a>
                    <div class="flex-col">
                        <span>Group: {{ $groupName }}</span>
                    </div>
                    <div class="flex-col">
                        <div class="flex-between new-team-controls">
                            <span>Team: </span>
                            <div class="input-container invalid">
                                <input name="teamName" id="team_name" type="text" autocomplete="off" value="">
                            </div>
                            <div class="button-container add-team"
                                 data-group="{{ $groupName }}"
                                 data-token="{{ csrf_token() }}"
                                 style="background-color: rgb(170, 170, 170);"
                            >Add</div>
                        </div>
                    </div>
                    <div class="flex-col">
                        <span>Teams: </span>
                        <div class="flex-col">
                            <ol>
                                <div class="team-list">
                                    @foreach($teams as $team)
                                        <li class="team-item list-item-enter-done">
                                            <span class="icon deleteTeam"
                                                  data-name="{{ $team['team_name'] }}"
                                                  data-token="{{ csrf_token() }}"
                                                  style="cursor: pointer;"
                                            >x</span>
                                            <span class="{{ $team['team_id'] }}">{{ $team['team_name'] }}</span>
                                        </li>
                                    @endforeach
                                </div>
                            </ol>
                        </div>
                    </div>
                    <div class="flex-col">
                        <div class="flex-between">
                            <span>Matches: </span>
                            <div class="button-container generate"
                                 data-group="{{ $groupName }}"
                                 data-token="{{ csrf_token() }}"
                                 @if (count($teams) <= 1)
                                    style="background-color: rgb(170, 170, 170);"
                                 @endif
                            >
                                <span>Generate</span>
                            </div>
                        </div>
                    </div>
                    <div class="match-list flex-col">
                        <div>
                            @foreach($matches as $match)
                                <div class="match-item flex-between list-item-enter-done">
                                    <span class="flex-grow match-team"
                                          aria-label="{{ $match['first_team_name'] }}">{{ $match['first_team_name'] }}</span>
                                    <div class="flex">
                                        <div class="input-container">
                                            <input name="matches.{{ $match['first_team_id'] }}.score"
                                                   class="match firstTeam" type="text" autocomplete="off"
                                                   data-group="{{ $groupName }}"
                                                   data-token="{{ csrf_token() }}"
                                                   data-teamId = "{{ $match['first_team_id'] }}"
                                                   value="{{ $match['first_team_result'] }}">

                                        :

                                            <input name="matches.{{ $match['second_team_id'] }}.score"
                                                   class="match secondTeam" type="text" autocomplete="off"
                                                   data-token="{{ csrf_token() }}"
                                                   data-teamId = "{{ $match['second_team_id'] }}"
                                                   value="{{ $match['second_team_result'] }}">
                                        </div>
                                    </div>
                                    <span class="flex-grow match-team"
                                          aria-label="{{ $match['second_team_name'] }}">{{ $match['second_team_name'] }}</span>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script src="http://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous">
    </script>
    <script>
        $(document).ready(function(){
            $('.button-container.add-team').click(function(){
                let groupName = $(this).data('group'),
                    token = $(this).data('token'),
                    teamName = $('#team_name').val();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                $.ajax({
                    data: {
                        "groupName": groupName,
                        "token": token,
                        'teamName': teamName,
                    },
                    url: "{{ url('/team') }}",
                    method: 'post',
                    success: function(result){
                        if (result !== "") {
                            $('.team-list').append(
                                '<li class="team-item list-item-enter-done">' +
                                '<span class="icon deleteTeam" style="cursor: pointer;">x</span>'+ result +
                                '</li>'
                            )
                            window.location.reload();
                        };
                    }
                });
            });
            $('.deleteTeam').click(function (){
                let teamName = $(this).data('name'),
                    token = $(this).data('token'),
                    el = this;
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                $.ajax({
                    data: {
                        "teamName": teamName,
                        "token": token,
                    },
                    url: "{{ url('/team') }}",
                    method: 'delete',
                    success: function(success) {
                        if (success === teamName) {
                            $(el).closest('li').remove();
                            window.location.reload();
                        }
                    }
                });
            });
            $("#team_name").keyup(function () {
                let value = $(this).val();
                if (value !== '') {
                    $('.input-container').removeClass('invalid')
                    $('.button-container.add-team').attr('style', '');
                    $(this).attr('value', value);
                } else {
                    $('.input-container').addClass('invalid')
                    $('.button-container.add-team').attr('style', 'background-color: #aaaaaa');
                }
            });
            $('.generate').click(function () {
                let groupName = $(this).data('group'),
                    token = $(this).data('token');
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                $.ajax({
                    data: {
                        "groupName": groupName,
                        "token": token,
                    },
                    url: "{{ url('/match/generate') }}",
                    method: 'post',
                    success: function(result){
                        window.location.reload();
                    }
                });
            });
            $('.match').change(function () {
                let token = $(this).data('token'),
                    score = $(this).val(),
                    firstTeamId,
                    secondTeamId,
                    firstTeamScore = null,
                    secondTeamScore = null;

                if (score === 0) return;

                if ($(this).hasClass('firstTeam')) {
                    firstTeamScore = score;
                    firstTeamId = $(this).data('teamid');
                    secondTeamId = $(this).next('input').data('teamid');
                } else {
                    secondTeamScore = score;
                    firstTeamId = $(this).prev('input').data('teamid');
                    secondTeamId = $(this).data('teamid');
                }

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                $.ajax({
                    data: {
                        "token": token,
                        "firstTeamId": firstTeamId,
                        "secondTeamId": secondTeamId,
                        "firstTeamScore": firstTeamScore,
                        "secondTeamScore": secondTeamScore,
                    },
                    url: "{{ url('/match/write') }}",
                    method: 'post',
                    success: function(success){
                        if (success !== "") {
                            $('.' + firstTeamId).prev('span').remove();
                            $('.' + secondTeamId).prev('span').remove();
                        }
                    }
                });
            });
        });
    </script>
</body>
</html>
