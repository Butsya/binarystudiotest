<?php
/**
 * Created by PhpStorm.
 * User: bucya
 * Date: 5/24/19
 * Time: 4:57 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Match extends Model
{
    /**
     * @inheritdoc
     */
    protected $table = 'matches';

    /**
     * @inheritdoc
     */
    protected $primaryKey = 'match_id';

    /**
     * @inheritdoc
     */
    public $timestamps = false;
}

