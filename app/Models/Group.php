<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    /**
     * @inheritdoc
     */
    protected $table = 'groups';

    /**
     * @inheritdoc
     */
    protected $primaryKey = 'group_id';

    /**
     * @inheritdoc
     */
    public $timestamps = false;
}
