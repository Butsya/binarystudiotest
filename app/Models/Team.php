<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    /**
     * @inheritdoc
     */
    protected $table = 'teams';

    /**
     * @inheritdoc
     */
    protected $primaryKey = 'team_id';

    /**
     * @inheritdoc
     */
    public $timestamps = false;
}
