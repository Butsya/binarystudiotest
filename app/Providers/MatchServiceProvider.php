<?php
/**
 * Created by PhpStorm.
 * User: bucya
 * Date: 5/24/19
 * Time: 3:12 PM
 */

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class MatchServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('App\Contracts\MatchRepositoryInterface', 'App\Repositories\MatchRepository');
    }
}
