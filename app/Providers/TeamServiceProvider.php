<?php
/**
 * Created by PhpStorm.
 * User: bucya
 * Date: 5/24/19
 * Time: 10:50 AM
 */

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class TeamServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('App\Contracts\TeamRepositoryInterface', 'App\Repositories\TeamRepository');
    }
}
