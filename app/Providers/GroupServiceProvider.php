<?php
/**
 * Created by PhpStorm.
 * User: bucya
 * Date: 5/24/19
 * Time: 10:27 AM
 */

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class GroupServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('App\Contracts\GroupRepositoryInterface', 'App\Repositories\GroupRepository');
    }
}
