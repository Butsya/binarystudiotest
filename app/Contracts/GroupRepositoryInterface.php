<?php
/**
 * Created by PhpStorm.
 * User: skozar
 * Date: 22.05.19
 * Time: 20:46
 */

namespace App\Contracts;

interface GroupRepositoryInterface
{
    /**
     * Save new group.
     *
     * @return string|null
     */
    public function save(): ?string;

    /**
     * Return all groups name.
     *
     * @return array
     */
    public function getAllGroupsName(): array;

    /**
     * Delete group by name.
     *
     * @param string $name
     * @return string
     */
    public function delete(string $name): string;

    /**
     * Get group id by group name.
     *
     * @param string $groupName
     * @return int
     */
    public function getIdByGroupName(string $groupName): int;
}
