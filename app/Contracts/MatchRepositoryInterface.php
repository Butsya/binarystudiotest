<?php
/**
 * Created by PhpStorm.
 * User: bucya
 * Date: 5/24/19
 * Time: 3:10 PM
 */

namespace App\Contracts;


interface MatchRepositoryInterface
{
    /**
     * Generate matches for current group.
     *
     * @param string $groupName
     *
     * @return void
     */
    public function generateMatches(string $groupName): void;

    /**
     * Return all generated matches for current group.
     *
     * @param string $groupName
     *
     * @return array
     */
    public function getAllMatches(string $groupName): array;

    /**
     * Write result of match.
     *
     * @param array $result
     * @param int $firstTeamId
     * @param int $secondTeamId
     * @return string
     */
    public function writeResult(array $result, int $firstTeamId, int $secondTeamId): string;
}
