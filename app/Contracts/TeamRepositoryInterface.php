<?php
/**
 * Created by PhpStorm.
 * User: bucya
 * Date: 5/24/19
 * Time: 10:44 AM
 */

namespace App\Contracts;


interface TeamRepositoryInterface
{
    /**
     * Add team.
     *
     * @param string $name
     * @param int $groupId
     * @return string
     */
    public function add(string $name, int $groupId): string;

    /**
     * Return all teams of current group.
     *
     * @param int $groupId
     * @return array
     */
    public function getList(int $groupId): array;

    /**
     * Delete team by name.
     *
     * @param string $name
     * @return string
     */
    public function delete(string $name): string;

    /**
     * Get team name by id.
     *
     * @param int $teamId
     * @return string
     */
    public function getTeamNameById(int $teamId): string;
}
