<?php

namespace App\Http\Controllers;

use App\Contracts\GroupRepositoryInterface;

class GroupController extends Controller
{
    /**
     * @var GroupRepositoryInterface
     */
    private $groupRepository;

    /**
     * @param GroupRepositoryInterface $groupRepository
     */
    public function __construct(GroupRepositoryInterface $groupRepository)
    {
        $this->groupRepository = $groupRepository;
    }

    /**
     * Show list of groups.
     */
    public function show()
    {
        return view('groups', ['groups'=> $this->groupRepository->getAllGroupsName()]);
    }

    /**
     * Add new group.
     *
     * @return string
     */
    public function add(): string
    {
        return $this->groupRepository->save();
    }

    /**
     * Delete group.
     *
     * @param string $groupName
     * @return string
     */
    public function delete(string $groupName): string
    {
        return $this->groupRepository->delete($groupName);
    }
}
