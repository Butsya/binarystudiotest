<?php

namespace App\Http\Controllers;

use App\Contracts\MatchRepositoryInterface;
use Illuminate\Http\Request;

class MatchController extends Controller
{
    /**
     * @var MatchRepositoryInterface
     */
    private $matchRepository;

    public function __construct(MatchRepositoryInterface $matchRepository)
    {
        $this->matchRepository = $matchRepository;
    }

    /**
     * Generate matches.
     *
     * @return void
     */
    public function generate(): void
    {
        $this->matchRepository->generateMatches($this->getGroupName());
    }

    /**
     * Write result of match.
     *
     * @return string
     */
    public function write(): string
    {
        $result['first'] = request()->get('firstTeamScore');
        $result['second'] = request()->get('secondTeamScore');
        $firstTeamId = request()->get('firstTeamId');
        $secondTeamId = request()->get('secondTeamId');

        return $this->matchRepository->writeResult($result, $firstTeamId, $secondTeamId);
    }

    /**
     * Return current group name.
     *
     * @return string
     */
    private function getGroupName(): string
    {
        return request()->get('groupName');
    }
}
