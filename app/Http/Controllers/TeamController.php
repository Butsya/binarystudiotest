<?php

namespace App\Http\Controllers;

use App\Contracts\GroupRepositoryInterface;
use App\Contracts\MatchRepositoryInterface;
use App\Contracts\TeamRepositoryInterface;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    /**
     * @var TeamRepositoryInterface
     */
    private $teamRepository;

    /**
     * @var GroupRepositoryInterface
     */
    private $groupRepository;

    /**
     * @var MatchRepositoryInterface
     */
    private $matchRepository;

    /**
     * @param TeamRepositoryInterface $teamRepository
     * @param GroupRepositoryInterface $groupRepository
     * @param MatchRepositoryInterface $matchRepository
     */
    public function __construct(
        TeamRepositoryInterface $teamRepository,
        GroupRepositoryInterface $groupRepository,
        MatchRepositoryInterface $matchRepository
    ) {
        $this->teamRepository = $teamRepository;
        $this->groupRepository = $groupRepository;
        $this->matchRepository = $matchRepository;
    }

    /**
     * Show list of groups.
     */
    public function show()
    {
        $groupName = substr(request()->getPathInfo(), -1);

        return view('teams', [
                'groupName' => $groupName,
                'teams' => $this->teamRepository->getList($this->groupRepository->getIdByGroupName($groupName)),
                'matches' => $this->matchRepository->getAllMatches($groupName),
            ]
        );
    }

    /**
     * Add team.
     *
     * @return string
     */
    public function add(): string
    {
        $groupId = $this->groupRepository->getIdByGroupName(request()->get('groupName'));
        $teamName = request()->get('teamName');

        return $this->teamRepository->add($teamName, $groupId);
    }

    public function delete()
    {
        $teamName = request()->get('teamName');

        return $this->teamRepository->delete($teamName);
    }
}
