<?php
/**
 * Created by PhpStorm.
 * User: bucya
 * Date: 5/24/19
 * Time: 3:13 PM
 */

namespace App\Repositories;

use App\Contracts\GroupRepositoryInterface;
use App\Contracts\MatchRepositoryInterface;
use App\Contracts\TeamRepositoryInterface;
use App\Models\Match;

class MatchRepository implements MatchRepositoryInterface
{
    /**
     * @var TeamRepositoryInterface
     */
    private $teamRepository;

    /**
     * @var GroupRepositoryInterface
     */
    private $groupRepository;

    /**
     * @param TeamRepositoryInterface $teamRepository
     * @param GroupRepositoryInterface $groupRepository
     */
    public function __construct(
        TeamRepositoryInterface $teamRepository,
        GroupRepositoryInterface $groupRepository
    ) {
        $this->teamRepository = $teamRepository;
        $this->groupRepository = $groupRepository;
    }

    /**
     * @inheritdoc
     */
    public function generateMatches(string $groupName): void
    {
        $groupId = $this->groupRepository->getIdByGroupName($groupName);
        $teams = $this->teamRepository->getList($groupId);
        $count = count($teams);
        if ($count >= 1) {
            for ($i = 0; $i < $count; $i++) {
                for ($j = $i+1; $j < $count; $j++) {
                    $match = new Match();
                    $match->first_team_id = $teams[$i]->team_id;
                    $match->second_team_id = $teams[$j]->team_id;
                    $match->group_id = $groupId;
                    try {
                        $match->save();
                    } catch (\Throwable $exception) {
                        continue;
                    }
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function getAllMatches(string $groupName): array
    {
        $groupId = $this->groupRepository->getIdByGroupName($groupName);

        $matches = Match::where('group_id', '=', $groupId)->get()->toArray();

        foreach ($matches as &$match) {
            $match['first_team_name'] = $this->teamRepository->getTeamNameById($match['first_team_id']);
            $match['second_team_name'] = $this->teamRepository->getTeamNameById($match['second_team_id']);
        }

        return $matches;
    }

    /**
     * @inheritdoc
     */
    public function writeResult(array $result = [], int $firstTeamId, int $secondTeamId): string
    {
        if (count($result) !== 0) {
            $match = Match::where([
                ['first_team_id', '=', $firstTeamId],
                ['second_team_id', '=', $secondTeamId],
            ])->get()->get(0);

            if ($result['first'] !== null) {
                $teamId = request()->get('firstTeamId');
                $match->first_team_result = $result['first'];
            } else {
                $teamId = request()->get('secondTeamId');
                $match->second_team_result = $result['second'];
            }

            return $match->save();
        }
        return "";
    }
}
