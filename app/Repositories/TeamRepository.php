<?php
/**
 * Created by PhpStorm.
 * User: bucya
 * Date: 5/24/19
 * Time: 10:44 AM
 */

namespace App\Repositories;

use App\Contracts\TeamRepositoryInterface;
use App\Models\Team;

class TeamRepository implements TeamRepositoryInterface
{
    /**
     * Add team.
     *
     * @param string $name
     * @param int $groupId
     * @return string
     */
    public function add(string $name, int $groupId): string
    {
        $team = new Team();
        $team->team_name = $name;
        $team->group_id = $groupId;

        $team->save();

        return $name;
    }

    /**
     * Return all teams of current group.
     *
     * @param int $groupId
     * @return array
     */
    public function getList(int $groupId): array
    {
        $teams = Team::all(['*'])->all();

        return $teams;
    }

    /**
     * Delete team by name.
     *
     * @param string $name
     * @return string
     */
    public function delete(string $name): string
    {
        $team = Team::where('team_name', '=', $name)->get()->get(0);
        $count = Team::destroy([$team->getAttribute('team_id')]);

        if ($count !== 0) {
            return $name;
        }

        return '';
    }

    /**
     * @inheritdoc
     */
    public function getTeamNameById(int $teamId): string
    {
        return Team::find($teamId)->team_name;
    }
}
