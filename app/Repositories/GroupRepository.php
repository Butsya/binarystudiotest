<?php
/**
 * Created by PhpStorm.
 * User: bucya
 * Date: 5/22/19
 * Time: 11:45 AM
 */

namespace App\Repositories;

use App\Contracts\GroupRepositoryInterface;
use App\Models\Group;

class GroupRepository implements GroupRepositoryInterface
{
    /**
     * @var array
     */
    private $groups = [
        'A',
        'B',
        'C',
        'D',
        'E',
        'F',
        'G',
        'H',
        'I',
        'J',
        'K',
        'L',
        'M',
        'N',
        'O',
        'P',
        'Q',
        'R',
        'S',
        'T',
        'U',
        'V',
        'W',
        'X',
        'Y',
        'Z',
    ];

    /**
     * Save new group.
     *
     * @return string
     */
    public function save(): string
    {
        $groupName = '';

        if (count($this->getAllGroupsName()) !== 26) {
            $group = new Group();
            $groupName = $this->getAvailableGroupName();
            $group->group_name = $groupName;
            $group->group_id = $this->getIdByGroupName($groupName);

            $group->save();
        }

        return $groupName;
    }

    /**
     * Return all groups name.
     *
     * @return array
     */
    public function getAllGroupsName(): array
    {
        $existedGroupsNames = [];

        foreach (Group::all(['group_name'])->all() as $group) {
            $existedGroupsNames[] = $group->group_name;
        }

        return $existedGroupsNames;
    }

    /**
     * Delete group by name.
     *
     * @param string $name
     * @return string
     */
    public function delete(string $name): string
    {
        $count = Group::destroy([$this->getIdByGroupName($name)]);
        if ($count !== 0) {
            return $name;
        }

        return '';
    }

    /**
     * Return available name for group.
     *
     * @return string|null
     */
    private function getAvailableGroupName(): ?string
    {
        $availableGroupNames = array_diff($this->groups, $this->getAllGroupsName());

        return count($availableGroupNames) === 0 ? null : array_shift($availableGroupNames);
    }

    /**
     * @inheritdoc
     */
    public function getIdByGroupName(string $groupName): int
    {
        return array_search($groupName, $this->groups);
    }
}
